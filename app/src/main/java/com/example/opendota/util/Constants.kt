package com.example.opendota.util

object Constants {
    const val REST_TIMEOUT = 25.toLong()
    const val GENERAL_LOG_APP_TAG = "OPEN_DOTA_LOG"


    /* REST String Responses */
    const val RESPONSE_SERVICE_OK = 200
    const val BAD_REQUEST = 400
    const val NOT_FOUND = 404
    const val INT_SRV_ERROR = 500
    const val UNAUTHORIZED = 401

    const val INTENT_VALUE = "intentValue"
}