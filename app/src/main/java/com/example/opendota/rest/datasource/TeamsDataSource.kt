package com.example.opendota.rest.datasource

import com.example.opendota.model.team.TeamBody
import com.example.opendota.model.team.TeamMatchesBody
import com.example.opendota.rest.Result

interface TeamsDataSource {
    suspend fun listTeams(): Result<List<TeamBody>>

    suspend fun listTeamMatches(input:Int): Result<List<TeamMatchesBody>>
}