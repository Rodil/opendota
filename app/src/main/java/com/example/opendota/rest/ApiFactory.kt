package com.example.opendota.rest

import com.example.opendota.util.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiFactory {
    private const val API_BASE_URL = "https://api.opendota.com"
    private var servicesApiInterface: ApiService? = null

    private val kwClient = OkHttpClient().newBuilder()
        //.addInterceptor(authInterceptor)
        .readTimeout(Constants.REST_TIMEOUT, TimeUnit.SECONDS)
        .connectTimeout(Constants.REST_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(Constants.REST_TIMEOUT, TimeUnit.SECONDS)
        .build()

    fun build(): ApiService? {
        var builder: Retrofit.Builder = Retrofit.Builder()
            .client(kwClient)
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

        var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.addInterceptor(interceptor())

        var retrofit: Retrofit = builder.client(httpClient.build()).build()
        servicesApiInterface = retrofit.create(
            ApiService::class.java
        )

        return servicesApiInterface as ApiService
    }

    private fun interceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }
}