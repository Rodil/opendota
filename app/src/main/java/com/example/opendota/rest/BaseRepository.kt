package com.example.opendota.rest

import android.util.Log
import com.example.opendota.util.AppUtils
import com.example.opendota.util.Constants
import com.kontiwi.admin.model.ErrorObjectResponse
import com.kontiwi.admin.model.NetworkMessage
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException


open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): Result<T> {

        return safeApiResult(call)


    }

    private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>): Result<T> {

        try {
            val response = call.invoke()


            Log.i(Constants.GENERAL_LOG_APP_TAG, response.raw().toString())
            Log.i(Constants.GENERAL_LOG_APP_TAG, response.message())
            Log.i(Constants.GENERAL_LOG_APP_TAG, response.body().toString())
            Log.i(Constants.GENERAL_LOG_APP_TAG, response.raw().networkResponse.toString())
            if (response.isSuccessful) return Result.Success(response.body()!!)

            var jsonObject = JSONObject(response.errorBody()?.string())

            val message: ErrorObjectResponse = AppUtils.fromJson(jsonObject.toString())

            var networkMessage = NetworkMessage("", 0)
            if (message.exception.exceptionMessage.isNotEmpty()) {
                networkMessage = NetworkMessage(message.exception.exceptionMessage, response.raw().code)
            }
            return Result.Error(networkMessage)
        } catch (e: IOException) {

            return Result.Error(NetworkMessage("", 0))
        }
    }


}
sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val exception: NetworkMessage) : Result<Nothing>()
}