package com.example.opendota.rest

import com.example.opendota.model.team.TeamBody
import com.example.opendota.model.team.TeamMatchesBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.Path
import retrofit2.http.Url

interface ApiService {

    @GET("/api/teams")
    suspend fun listTeams(): Response<List<TeamBody>>

    @GET("/api/teams/{input}/matches")
    suspend fun listMatchs(@Path("input") input:Int): Response<List<TeamMatchesBody>>


}