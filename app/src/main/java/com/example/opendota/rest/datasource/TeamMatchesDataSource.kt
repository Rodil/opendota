package com.example.opendota.rest.datasource

import com.example.opendota.model.team.TeamBody
import com.example.opendota.model.team.TeamMatchesBody
import com.example.opendota.rest.Result

interface TeamMatchesDataSource {
    suspend fun listTeamMatches(): Result<List<TeamMatchesBody>>
}