package com.example.opendota.viewmodel.team

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.opendota.model.team.TeamBody
import com.example.opendota.rest.datasource.TeamsDataSource
import androidx.lifecycle.viewModelScope
import com.example.opendota.model.team.TeamMatchesBody
import com.example.opendota.rest.Result
import com.kontiwi.admin.model.Message
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class TeamViewModel(private val repository: TeamsDataSource) : ViewModel() {

    private val _teams = MutableLiveData<List<TeamBody>>()
    val teams: LiveData<List<TeamBody>> = _teams

    private val _teamMatchess = MutableLiveData<List<TeamMatchesBody>>()
    val matches: LiveData<List<TeamMatchesBody>> = _teamMatchess

    private val _isViewLoading = MutableLiveData<Boolean>()
    val isViewLoading: LiveData<Boolean> = _isViewLoading


    private val _orderStateChanged = MutableLiveData<Message>()
    val orderStateChanged: LiveData<Message> = _orderStateChanged

    private val _onMessageError = MutableLiveData<Any>()
    val onMessageError: LiveData<Any> = _onMessageError

    private val _isEmptyList = MutableLiveData<Boolean>()
    val isEmptyList: LiveData<Boolean> = _isEmptyList


    fun cancel() {
        viewModelScope.cancel()
    }

    fun doListTeams() {
        _isViewLoading.postValue(true)
        viewModelScope.launch {
            val result: Result<List<TeamBody>> = withContext(Dispatchers.IO) {
                repository.listTeams()
            }
            _isViewLoading.postValue(false)
            when (result) {
                is Result.Success -> {
                    _teams.value = result.data
                    println("DATA:: "+teams.value.toString())
                }
                is Result.Error -> {
                    _onMessageError.postValue(result.exception)

                }
            }


        }


    }

    fun doListTeamsMatches(input: Int) {
        _isViewLoading.postValue(true)
        viewModelScope.launch {
            val result: Result<List<TeamMatchesBody>> = withContext(Dispatchers.IO) {
                repository.listTeamMatches(input)
            }
            _isViewLoading.postValue(false)
            when (result) {
                is Result.Success -> {
                    _teamMatchess.value = result.data
                    println("DATA:: "+teams.value.toString())
                }
                is Result.Error -> {
                    _onMessageError.postValue(result.exception)

                }
            }
        }

    }


}