package com.example.opendota.viewmodel.team

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.opendota.rest.datasource.TeamsDataSource

class TeamViewModelFactory (private val respository:TeamsDataSource):ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TeamViewModel(respository) as T
    }

}