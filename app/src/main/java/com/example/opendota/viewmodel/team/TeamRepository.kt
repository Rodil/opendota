package com.example.opendota.viewmodel.team

import com.example.opendota.model.team.TeamBody
import com.example.opendota.model.team.TeamMatchesBody
import com.example.opendota.rest.ApiFactory
import com.example.opendota.rest.ApiService
import com.example.opendota.rest.BaseRepository
import com.example.opendota.rest.Result
import com.example.opendota.rest.datasource.TeamsDataSource

class TeamRepository : BaseRepository(), TeamsDataSource {
    private var client: ApiService = ApiFactory.build()!!

    override suspend fun listTeams(): Result<List<TeamBody>> {
        return  safeApiCall(call = {client.listTeams()})
    }

    override suspend fun listTeamMatches(input:Int): Result<List<TeamMatchesBody>> {
        return  safeApiCall(call = {client.listMatchs(input)})
    }
}