package com.example.opendota.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.opendota.R
import com.example.opendota.model.team.TeamBody
import com.example.opendota.model.team.TeamMatchesBody
import kotlinx.android.synthetic.main.item_match.view.*
import kotlinx.android.synthetic.main.item_team.view.*
import kotlinx.android.synthetic.main.item_team.view.iviLogo
import kotlinx.android.synthetic.main.item_team.view.tviName

class TeamMatchesAdapter(private val listener: ((TeamMatchesBody) -> Unit)? = null
) : RecyclerView.Adapter<TeamMatchesAdapter.TeamMatchHolder>() {


    var arrayMatchTeams: List<TeamMatchesBody> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamMatchHolder {


        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_match, parent, false)
        return TeamMatchHolder(view)
    }

    override fun getItemCount(): Int = arrayMatchTeams.size

    override fun onBindViewHolder(holder: TeamMatchHolder, position: Int) {
        val match = arrayMatchTeams[position]
        holder.bindTeams(match)
    }


    class TeamMatchHolder(private val view: View) : RecyclerView.ViewHolder(view){

        fun bindTeams(match: TeamMatchesBody) {
            if( match.radiant_win==true && match.radiant==true ){
                //victory
                view.tviName.text=match.opposing_team_name
                Glide.with(itemView.context)
                    .load(match.opposing_team_logo)
                    .placeholder(R.drawable.ogteam)
                    .error(R.drawable.ic_launcher_background)
                    .into(view.iviLogo)
                view.tviLeague.text=match.league_name
                view.tviResult.text= "Win"
                view.tviResult.setTextColor(Color.rgb(76, 175, 80))

            }else if(match.radiant_win==false && match.radiant==false){
                //victory
                view.tviName.text=match.opposing_team_name
                Glide.with(itemView.context)
                    .load(match.opposing_team_logo)
                    .placeholder(R.drawable.ogteam)
                    .error(R.drawable.ic_launcher_background)
                    .into(view.iviLogo)
                view.tviLeague.text=match.league_name
                view.tviResult.text= "Win"
                view.tviResult.setTextColor(Color.rgb(76, 175, 80))

            }else{
                //lose
                view.tviName.text=match.opposing_team_name
                Glide.with(itemView.context)
                    .load(match.opposing_team_logo)
                    .placeholder(R.drawable.ogteam)
                    .error(R.drawable.ic_launcher_background)
                    .into(view.iviLogo)
                view.tviLeague.text=match.league_name
                view.tviResult.text= "Lose"
                view.tviResult.setTextColor(Color.rgb(244, 67, 54))

            }



        }


    }


}