package com.example.opendota.adapter


import android.content.Context
import android.content.Intent
import android.provider.SyncStateContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.opendota.R
import com.example.opendota.activity.home.MatchActivity
import com.example.opendota.model.team.TeamBody
import com.example.opendota.util.Constants
import kotlinx.android.synthetic.main.item_team.view.*


class TeamAdapter(val context: Context) : RecyclerView.Adapter<TeamAdapter.TeamHolder>() {


    var arrayTeams: List<TeamBody> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamHolder {


        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_team, parent, false)
        return TeamHolder(view)
    }

    override fun getItemCount(): Int = arrayTeams.size

    override fun onBindViewHolder(holder: TeamHolder, position: Int) {
        val team = arrayTeams[position]
        holder.bindTeams(team)
        holder.itemView.iviLogo.setOnClickListener {
            val intent = Intent(context, MatchActivity::class.java)
            intent.putExtra(
                Constants.INTENT_VALUE,
                team
            )
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }


    class TeamHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bindTeams(team: TeamBody) {
            view.tviName.text = team.name
            Glide.with(itemView.context)
                .load(team.logo_url)
                .placeholder(R.drawable.ogteam)
                .error(R.drawable.ic_launcher_background)
                .into(view.iviLogo)


        }


    }


}