package com.example.opendota.activity.home

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.opendota.R
import com.example.opendota.adapter.TeamAdapter
import com.example.opendota.di.Injection
import com.example.opendota.model.team.TeamBody
import com.example.opendota.viewmodel.team.TeamViewModel
import com.kontiwi.admin.model.Message
import com.kontiwi.admin.model.NetworkMessage
import kotlinx.android.synthetic.main.activity_main.*

class TeamActivity : AppCompatActivity() {

    private lateinit var viewModel: TeamViewModel
    private lateinit var adapter: TeamAdapter
    private var ishangedStatus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setup()


    }

    private fun setup() {
        //Setup RecyclerView

        adapter = TeamAdapter(applicationContext)
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rviTeams.layoutManager = layoutManager
        rviTeams.adapter = adapter

        //Setup ViewModel
        viewModel = ViewModelProvider(
            this,
            Injection.provideTeamViewModelFactory()
        ).get(TeamViewModel::class.java)

        viewModel.teams.observe(this, renderTeams)
        viewModel.orderStateChanged.observe(this, changeStatus)
        viewModel.onMessageError.observe(this, onMessageErrorObserver)
        viewModel.doListTeams()

        //check
        Toast.makeText(
                applicationContext,
        "Data " + renderTeams.toString() + " :::: " + adapter.arrayTeams.toString(),
        Toast.LENGTH_LONG
        ).show()


    }


    //region
    private val renderTeams = Observer<List<TeamBody>> {
        adapter.arrayTeams = it

    }

    private val changeStatus = Observer<Message> {
        ishangedStatus = true
        viewModel.doListTeams()
    }
    private val onMessageErrorObserver = Observer<Any> {

        val error = it as NetworkMessage

    }

    //end region

/*
    private val isViewLoadingObserver = Observer<Boolean> {

        if (it) {
            if (!refreshLayout.isRefreshing) {
                this.showLoadingView(
                    resources.getString(R.string.login_loading),
                    SyncStateContract.Constants.EnumViewLoading.VIEW_BACKGROUND_WHITE
                )
            }

        } else {
            refreshLayout.isRefreshing = false
            this.hideLoadingView()
        }

    }
    */

}