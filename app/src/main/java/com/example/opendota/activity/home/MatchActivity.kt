package com.example.opendota.activity.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.opendota.R
import com.example.opendota.adapter.TeamMatchesAdapter
import com.example.opendota.di.Injection
import com.example.opendota.model.team.TeamBody
import com.example.opendota.model.team.TeamMatchesBody
import com.example.opendota.util.Constants
import com.example.opendota.viewmodel.team.TeamViewModel
import com.kontiwi.admin.model.Message
import com.kontiwi.admin.model.NetworkMessage
import kotlinx.android.synthetic.main.activity_match.*
import kotlinx.android.synthetic.main.item_team.view.*

class MatchActivity : AppCompatActivity() {
    private lateinit var match: TeamBody

    private lateinit var viewModel: TeamViewModel
    private lateinit var adapter: TeamMatchesAdapter
    private var ishangedStatus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match)

        intent.getParcelableExtra<TeamBody>(Constants.INTENT_VALUE)?.let {
            this.match=it
            setup()
        }
        Toast.makeText(applicationContext, ""+this.match.name+ "\n"+this.match.rating, Toast.LENGTH_SHORT).show()

        tviNameTeam.text=this.match.name
        Glide.with(applicationContext)
            .load(this.match.logo_url)
            .placeholder(R.drawable.ogteam)
            .error(R.drawable.ic_launcher_background)
            .into(ivilogoTeam)
        tviRating.text=this.match.rating.toString()
        tviWins.text=this.match.wins.toString()
        tviLosses.text=this.match.losses.toString()




    }

    private fun setup() {
        //Setup RecyclerView

        adapter = TeamMatchesAdapter()
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rviMatches.layoutManager = layoutManager
        rviMatches.adapter = adapter

        //Setup ViewModel
        viewModel = ViewModelProvider(
            this,
            Injection.provideTeamViewModelFactory()
        ).get(TeamViewModel::class.java)

        viewModel.matches.observe(this, renderTeams)
        viewModel.orderStateChanged.observe(this, changeStatus)
        viewModel.onMessageError.observe(this, onMessageErrorObserver)
        viewModel.doListTeamsMatches(this.match.team_id)

        //check


    }

    //region
    private val renderTeams = Observer<List<TeamMatchesBody>> {
        adapter.arrayMatchTeams = it

    }

    private val changeStatus = Observer<Message> {
        ishangedStatus = true
        viewModel.doListTeams()
    }
    private val onMessageErrorObserver = Observer<Any> {

        val error = it as NetworkMessage

    }

    //end region
}