package com.example.opendota.model.team

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TeamBody(
    val team_id: Int,
    val rating: Float,
    val wins: Int,
    val losses: Int,
    val last_match_time: Int,
    val name: String,
    val tag: String,
    val logo_url: String
) : Parcelable