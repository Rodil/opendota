package com.example.opendota.model.team

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TeamMatchesBody(
    val match_id:Long,
    val radiant_win: Boolean,
    val radiant: Boolean,
    val duration: Int,
    val start_timeInt: Int,
    val leagueid: Int,
    val league_name: String,
    val cluster: Int,
    val opposing_team_id: Int,
    val opposing_team_name: String,
    val opposing_team_logo: String
) : Parcelable

