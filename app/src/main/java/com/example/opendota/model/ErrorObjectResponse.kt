package com.kontiwi.admin.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ErrorObjectResponse(
    var exception: ErrorObject

) : Parcelable  {
    companion object{
        fun toErrorObject(response: ErrorObject): ErrorObject {
            return ErrorObject(
                response.exceptionCategory,
                response.exceptionCode,
                response.exceptionMessage,
                response.exceptionDetail,
                response.exceptionSeverity)
        }
    }
}



