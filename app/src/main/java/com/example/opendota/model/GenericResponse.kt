package com.kontiwi.admin.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class GenericResponse(
    val message: Message
) : Parcelable
