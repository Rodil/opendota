package com.kontiwi.admin.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Message(
    val estado: Boolean,
    val message: String
) : Parcelable