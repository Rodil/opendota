package com.kontiwi.admin.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ErrorObject(
        var exceptionCategory: String = "",
        var exceptionCode: String = "",
        var exceptionMessage: String = "",
        var exceptionDetail: String = "",
        var exceptionSeverity: String = ""
) : Parcelable

