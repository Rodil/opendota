package com.example.opendota.di

import androidx.lifecycle.ViewModelProvider
import com.example.opendota.rest.datasource.TeamsDataSource
import com.example.opendota.viewmodel.team.TeamRepository
import com.example.opendota.viewmodel.team.TeamViewModelFactory


object Injection {

    //region team
    private val teamsDataSource: TeamsDataSource =
        TeamRepository()
    private val teamViewModelFactory = TeamViewModelFactory(teamsDataSource)


    fun provideTeamViewModelFactory(): ViewModelProvider.Factory {
        return teamViewModelFactory
    }
    //endregion

}